
## Welcome! {{< icon mask >}}

I am António / *Larus Argentatus*. A young militant for Climate Justice and a Security Officer and Trainer from Portugal.

Sometimes I like to write, mostly about Security Culture, Analysis and Strategy.

---

You can contact me using:
- {{< icon mastodon >}} **Mastodon**: [@larusargentatus@infosec.exchange](https://infosec.exchange/@larusargentatus) ;
- {{< icon email >}}  **E-mail**: [larus-argentatus@riseup.net](mailto:larus-argentatus@riseup.net) ;
- {{< icon pgp >}} **PGP Key**: [OpenPGP KeyServer](https://keys.openpgp.org/vks/v1/by-fingerprint/7526ADB57085445B93AFA69386D89A55D602DFB2) / [Local File](/keys/larus-argentatus.asc) ;
  - {{< icon fingerprint >}} **Fingerprint**: `7526 ADB5 7085 445B 93AF A693 86D8 9A55 D602 DFB2` ;
