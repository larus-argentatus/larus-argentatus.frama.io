---
layout: single
title: State of Journalist Security in Portugal (2024) - Part 1
date: 2024-04-05
---

{{<image src="/images/posts/GG_14_03_2024_LeonardoNegrao_4.jpg" alt="General Strike of Journalists in Lisbon on the 14th of March 2024" caption="*General Strike of Journalists in Lisbon on the 14th of March 2024*" class="aligncenter" >}}

*This is an article about the current security culture and threats for Portugal based Journalists - it tries to present an overview of digital and physical threats, a critique of current (non-)practices and a possible way forward.*

## Recent Attacks

As a starting point, I would like to explain some of the recent (*last 5 years*) attacks that paint a picture of some of the threats.

### During Protests

On the *3<sup>rd</sup> of February 2024*, while reporting on a far-right demonstration and its counter-protest, 2 **identified** journalists (from [Fumaça](https://fumaca.pt) and [Setenta e Quatro](https://setentaequatro.pt/)) were attacked by the Intervention Corp of the Police's Special Unit (CI/UEP). This happened during a charge by the same unit against Anti-Fascists protesting. [^1]

At the end of last year, on the *31<sup>st</sup> of December*, while covering a protest against the cruelty of the Portuguese prison system and in solidarity with Palestine, 1 [Fumaça](https://fumaca.pt) journalist was also beaten by agents of the Rapid Intervention Team (EIR/PSP) while documenting an arrest. [^2]

### During Political Events

Recently, on the *16<sup>th</sup> of February 2024*, while attending a student event organized by the far-right party Chega, 1 [Expresso](https://expresso.pt) was forcibly dragged out of the room by some of the students inside.[^3]

Of the same party, we have other incidents. One of them was on the *8<sup>th</sup> of January* a freelance journalist was arbitrarily denied access to the party's convention that would occur some days later.[^4]

### Infrastructure Attacks

In the first 2 months of 2022, 2 major media conglomerate websites and in 1 case **internal data** were attacked by one ransomware gang, `Lapsus$`.

While both cases may be interesting to explore on the side of availability of the news I will focus on the first since the attackers publicly acknowledged stealing internal data.

It happened on the *2<sup>nd</sup> of January*. The Imprensa media outlet was hit, bringing the websites of [Expresso](https://expresso.pt) and [SIC](https://sic.pt) offline. The internal data was also stolen. The attackers claimed to gain access to Imprensa's Amazon Web Services.[^5]

### Surveillance

Even though this case is a little older, from 2018, I believe it is still relevant to reflect. Even though the age and how scarcely these tactics have been employed by Security Forces in this country.

The incident happens between April and May of that year. During an investigation into corruption between a judicial worker and the football club Benfica.

The prosecutor ordered the police (PSP) to surveil 4 journalists, from [Sábado](https://www.sabado.pt/) [TVI](https://tvi.iol.pt/) [Visão](https://visao.pt/) and [SIC](https://sic.pt/). The methods included physical surveillance and documenting their routines, in one case accessing the bank records and accessing the private communications (SMS and call records), between them and alleged sources. The argument behind the surveillance was to **catch a leak from the process** since the 4 journalists had published articles using leaked information. [^6]

### Online Harassment

Lastly, I want to look at the rising trend of harassing journalists online.

The exponential rise of the far-right party Chega and Elon Musk taking over Twitter/X, even though they do not necessarily correlate, can be seen as one of the enabling agents. From the abstract threats of "cleaning up" journalists that investigate their shady practices to private messages and public tweets publicly threatening journalists, the list of individual offenses is immense.

In some cases we saw the connection of digital harasment with physical intimidation, when a journalist posted a photo at a restaurant, later to discover that someone went to the restaurant to see if she was there, fortunately, the photo was not posted live, but only some hours after they were there.

We can also look at the case of the shutting down, due to funding issues, of [Setenta e Quatro](https://setentaequatro.pt/). Setenta e Quatro, for the short time it was active, investigated Neo-Fascist groups, the hate Groups and ideologies inside the police and corruption cases. Most of their journalists would receive threats regularly on Twitter/X and the post of the shutdown was celebrated by everyone that hates independent journalism and Press Freedom.

---

*There are also plenty of attacks against journalists covering sports events, especially Football. From enraged fans after a loss to organized groups silencing the ones covering corruption inside the clubs. I am unsure of which security practices mitigate some of the physical threats, although some of the practices to be proposed later on can be used as a canvas to create safer sports journalism.*

---

The reality is not simple but:
- We can observe attacks in fields of **Civil Society**, **Political Events**, and **Corruption Cases**;
- On the digital side there are a plethora of different harasment techniques, while most of them do not materialize in instant damage, we have to look at the **emotional side** of harrasment, as well as the possibility of one day it **escalating** even more;
- We can also observe that overview over police/judiciary surveillance is almost non-existent and it is not even necessary to have authorization from a judge (allegedly);
- Portugal is a small country, journalism is under-funded and threats are not that common, while this may be true, no one knows what the future holds, and while today we can freely communicate and press freedom is still, minimally, upheld, tomorrow can be radically different;
- Sources and Journalists can be in grave danger if due diligence is not taken when contacting, interviewing and receiving information;


---

Thank you for reading! Soon I plan to release the next parts of this article. Any feedback is welcome either by e-mail (slower response) or by connecting on Mastodon.

<!--- Sources and Footnotes -->
[^1]: https://fumaca.pt/psp-agride-jornalistas-do-fumaca-e-setenta-e-quatro/
[^2]: https://fumaca.pt/jornalista-do-fumaca-agredido-a-bastonada-por-agente-da-psp/
[^3]: https://expresso.pt/politica/2024-01-16-Jornalista-do-Expresso-agredido-num-evento-universitario-com-o-lider-do-Chega-4a681746
[^4]: https://www.jn.pt/4634650015/erc-proibe-chega-de-discriminar-jornalistas-freelancers-no-congresso/
[^5]: https://www.reuters.com/business/media-telecom/portugals-impresa-media-outlets-hit-by-hackers-2022-01-03/
[^6]: https://www.sabado.pt/portugal/detalhe/ministerio-publico-mandou-psp-vigiar-e-fotografar-jornalistas?ref=hp_destaquetema
