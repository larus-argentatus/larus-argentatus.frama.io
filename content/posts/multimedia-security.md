---
layout: single
title: Security Resources
date: 2025-01-06
---

At [Fumaça](https://fumaca.pt), an independent investigative newsroom from Portugal, we are doing a very long security training, focused mostly in Digital Security. Most of it is based on the [ADIDS](https://www.level-up.cc/before-an-event/preparing-sessions-using-adids/) approach. As a way to develop further reflection and knowledge about the specific topic we are working on in a session, we have at the end a *Deepening the Lessons* part, were I (as the Trainer), provide other resources in a plethora of formats (Video, Audio, Books/Articles and sometimes Games), sometimes directly connected to the topic, other times marginally connected. This article serves as the public repository for those resources.

If you find this useful be free to use them. If you have any other resource suggestions ping me at any of the contact options below.

## Module 00 - Arrival

*This was the module with almost no content, still we dived into some basic concepts.*

**004 - Information Security 101**:
- To read: [Security by Obscurity](https://www.researchgate.net/profile/Elizabeth-Watkins-2/publication/316609881_%27Security_by_Obscurity%27_Journalists%27_Mental_Models_of_Information_Security/links/5907a60baca272116d3cb1ed/Security-by-Obscurity-Journalists-Mental-Models-of-Information-Security.pdf);
- To see: [A year of surveillance in France: a short satirical tale](https://framatube.org/videos/watch/efa9bca9-d545-4cf8-af91-9b5af8524642);
- To listen: [Digital Security for Journalists with Freedom of the Press Foundation](https://www.youtube.com/watch?v=JMupBC0IOMQ);

## Module 01 - Foundations

**101 - Threat Modeling**:
- To read: [User Personas for Privacy and Security](https://gusandrews.medium.com/user-personas-for-privacy-and-security-a8b35ae5a63b);
- To play: [Press Panic!](https://framagit.org/larus-argentatus/press-panic);

> Are you going to give a security training soon? Check out **Press Panic!** a game developed by me to ease into Threat Modeling.

**102 - Risk Analysis**:
- To read: [Risk assessments can make journalism safer](https://www.cjr.org/analysis/risk-assessments-can-make-journalism-safer.php);

## Module 02 - Personal Security

**201 - Doxxing and Harassment**:
- To read: [April Glaser's "13 security tips for journalists covering hate online"](https://journalistsresource.org/tip-sheets/reporting/13-security-tips-journalists-hate-online/);
- To see: [Sumsub - How to Stalk People Effectively and Legally Through OSINT](https://www.youtube.com/watch?v=IF3yQFtYRBY);
- To listen: [The Secure Dad Podcast - Doxxing: How to Protect Yourself](https://open.spotify.com/episode/5bJQy3beSyVbdhSEQaimKi);

**202 - Phishing and Targeted Attacks**:
- To read: [Practical Social Engineering](https://nostarch.com/practical-social-engineering) // [Sextortion Scammers Try to Scare People by Sending Photos of Their Homes](https://www.404media.co/sextortion-scammers-try-to-scare-people-by-sending-photos-of-their-homes/);
- To see: [Watch a CNN reporter get hacked](https://www.youtube.com/watch?v=yIG4kTJTZuY);
- TO listen: [Darknet Diaries - Alethe](https://darknetdiaries.com/episode/107/);

**203 - Passwords and Accounts**:
- To read: [Keep Calm and Log On - Gus Andrews](https://www.keepcalmlogon.com/);
- To see: [2FA: Two Factor Authentication - Computerphile](https://www.youtube.com/watch?v=ZXFYT-BG2So);
